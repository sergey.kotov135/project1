package org.example;

public final class HelperClass {

    private HelperClass() {
        System.out.println("Создан объект класса HelperClass");
    }

    public static void print(final String string) {
        System.out.print(string);
    }

    public static void println(final String string) {
        System.out.println(string);
    }
}
