package org.example;

/**
 * Hello world!
 *
 */
public final class App {
    private static final int ZERO = 5;

    public static void main(String[] args) {
        String str = ZERO + "";
        HelperClass.println("Hell World");
        HelperClass.print(str);
    }
}
